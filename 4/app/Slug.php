<?php

namespace App; //Para que funcione

class Slug
{
    protected $var;

    public function __construct($var){
        $this->var = $var;
    }

    public function render(){
        $slug = str_replace(" ","-",trim($this->var));
        return strtolower($slug);
    }
}