<?php


class Slug
{
    protected $var;

    public function __construct($var){
        $this->var = $var;
    }

    public function render(){
        $slug = str_replace(" ","-",$this->var);
        return strtolower($slug);
    }
}