<?php

use PHPUnit\Framework\TestCase; 
use App\Slug;

//Pruebe con: vendor\bin\phpunit 
class SlugTest extends TestCase{

    public function test_render(){

        $slug = new Slug("Hola soy nuevo");
        $this->assertEquals("hola-soy-nuevo", $slug->render()); //Método de phpunit para comparar si el resultado es el mismo... 

    }

    public function test_render_sin_espacios(){

        $slug = new Slug("    Hola soy nuevo     ");
        $this->assertEquals("hola-soy-nuevo", $slug->render()); 

    }

}