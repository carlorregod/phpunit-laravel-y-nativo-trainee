<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\Auth\RegisterController; //Llamada al controlador

class RegisterControllerTest extends TestCase
{

	//protected $register;

	public function setUp():void
	{
		$this->register= new RegisterController;
	}

    public function routeStatus()
    {
    	$response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_string()
    {
    	
    	$palabra = $this->register->setString("Hola");

        $this->assertEquals($this->register->getString(),"Hola");
    }
}
