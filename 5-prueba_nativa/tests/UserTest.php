<?php

use PHPUnit\Framework\TestCase;
use App\User;

class UserTest extends TestCase
{

	protected $user;

	public function setUp():void
	{
		$this->user= new User;
	} 
	public function test_i_can_the_name(){
		
		$this->user->setName("Carlos");
		
		$this->assertEquals($this->user->getName(),"Carlos");
	}


	public function test_i_can_the_lastname(){
		
		$this->user->setLastName("Orrego");
		
		$this->assertEquals($this->user->getLastName(),"Orrego");
	}

	public function test_i_can_the_email(){
		
		$this->user->setEmail("alguien@ejemplo.com");
		
		$this->assertEquals($this->user->getEmail(),"alguien@ejemplo.com");
	}

	public function test_i_can_the_fullname(){
		
		$this->user->setName("Carlos");
		$this->user->setLastName("Orrego");
		
		$this->assertEquals($this->user->getFullName(),"Carlos Orrego");
	}

	public function test_i_can_the_fullname_empty(){
		
		$this->assertEmpty($this->user->getFullName());
	}

	public function test_name_lastname_email_without_spaces(){

		$this->user->setName("   Carlos ");
		$this->user->setLastName( "Orrego  ");
		$this->user->setEmail(    "alguien@ejemplo.com  ");

		$this->assertEquals($this->user->getName(),"Carlos");
		$this->assertEquals($this->user->getLastName(),"Orrego");
		$this->assertEquals($this->user->getEmail(),"alguien@ejemplo.com");

	}

}