<?php

use PHPUnit\Framework\TestCase; //Se llama respecto al root del proyecto

//Pruebe con: vendor\bin\phpunit slugTest.php
class SlugTest extends TestCase{

    public function test_render(){
         /**
         * Ejemplo.
         *
         * @return OK
         */
        require "app/clase.php"; //Hay que referenciarla respecto al root del proyecto
        $slug = new Slug("Hola soy nuevo");
        $this->assertEquals("hola-soy-nuevo", $slug->render()); //Método de phpunit para comparar si el resultado es el mismo... 

    }

}

